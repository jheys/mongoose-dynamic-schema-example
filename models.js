var mongoose = require('mongoose')
    , thingModel = mongoose.model('Creature',
        new mongoose.Schema({
            name : String,
            description : String,
            custom : mongoose.Schema.Types.Mixed
        })
    );

exports.Thing = thingModel;
