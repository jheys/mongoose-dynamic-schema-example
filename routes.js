var models = require('./models')
    ,util = require('./util');

var getDocumentBody = function (req, doc){
    if (doc){
        return {
            thing : doc,
            update : req.protocol + '://' + req.host + ':' + req.app.get('port') + '/update/' + doc._id,
            remove : req.protocol + '://' + req.host + ':' + req.app.get('port') + '/remove/' + doc._id
        };
    }else{
        return {
            message : "document not found"
        };
    }
};

exports.index = function(req, res){
    models.Thing.find(function(err, docs){
        var list = [];
        var n = docs.length;
        for (var i = 0; i < n; i++){
            list.push(getDocumentBody(req, docs[i]));
        }
        res.send(list);
    });
};

exports.create = function(req, res){
    if (util.numMatchingFields(req.query, models.Thing.schema.tree) == 0){
        res.send({message:"no data"});
    }else{
        models.Thing.create(req.query, function(err, doc){
            if (err){
                res.send(err);
            }else{
                res.send(getDocumentBody(req, doc));
            }
        });
    }
}

exports.update = function (req, res){
    models.Thing.findByIdAndUpdate(req.params.id, req.query, function(err, doc){
        if (err){
            res.send(err);
        }else {
            res.send(getDocumentBody(req, doc));
        }
    });
};

exports.remove = function (req, res){
    models.Thing.findByIdAndRemove(req.params.id, function(err, doc){
        if (err){
            res.send(err);
        }else{
            res.send(getDocumentBody(req, doc));
        }
    });
};

