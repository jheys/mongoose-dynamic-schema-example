exports.isObjectEmpty = function(obj){
    for(var key in obj) {
        if (obj.hasOwnProperty(key)) {
            return false;
        }
    }
    return true;
}

exports.numMatchingFields = function(a, b){
    var n = 0;
    for(var key in a) {
        if (a.hasOwnProperty(key) && b.hasOwnProperty(key)) {
            n++;
        }
    }
    return n;
}